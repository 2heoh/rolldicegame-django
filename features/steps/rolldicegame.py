from behave import given, when, then
from django.urls import reverse


@given(u'player in game')
def step_impl(context):
    context.url = reverse("rolldicegame:play")


@when(u'dealer roll dice with one face')
def step_impl(context):
    context.response = context.test.client.post(context.url, {"player_amount": 1})


@then(u'player gets 6 bets')
def step_impl(context):
    context.test.assertContains(context.response, 'Player amount: 6')
