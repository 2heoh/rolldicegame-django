from domain.player import Player


class Game(object):
    player: Player

    def play(self):
        return self.player.win(6)

    def join(self, player):
        self.player = player
