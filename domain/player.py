class Player(object):
    def __init__(self):
        self.in_game = False
        self.amount = 1

    def get_amount(self):
        return self.amount

    def win(self, amount):
        self.amount = amount