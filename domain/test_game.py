from unittest import TestCase

from domain.game import Game
from domain.player import Player


class TestGame(TestCase):

    def test_player_has_1_chip(self):
        game = Game()
        player = Player()

        game.join(player)

        self.assertEqual(player.get_amount(), 1)

    def test_player_wins_6_bets(self):
        game = Game()
        player = Player()
        game.join(player)

        game.play()

        self.assertEqual(player.get_amount(), 6)
