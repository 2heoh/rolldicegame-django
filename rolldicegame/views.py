from django.shortcuts import render

from domain.game import Game
from domain.player import Player


def play(request):
    game = Game()
    player = Player()
    game.join(player)

    if request.POST:
        game.play()

    return render(request, "rolldicegame.html", context={"player_amount": player.get_amount()})
