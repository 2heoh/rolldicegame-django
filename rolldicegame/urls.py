from django.urls import path
from rolldicegame import views

app_name = 'rolldicegame'
urlpatterns = [
    path('play/', views.play, name='play'),
]
