from django import forms


class RollDiceGame(forms.Form):
    player_amount = forms.DecimalField(decimal_places=3)
