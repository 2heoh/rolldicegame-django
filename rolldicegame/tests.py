from django.test import TestCase, Client
from django.urls import reverse


class TestLengthConversion(TestCase):
    """
    This class contains tests that convert measurements from one
    unit of measurement to another.
    """

    def setUp(self):
        """
        This method runs before the execution of each test case.
        """
        self.client = Client()
        self.url = reverse("rolldicegame:play")

    def test_player_has_1_chip(self):
        response = self.client.get(self.url)

        self.assertContains(response, "value=\"1\"")

    def test_player_wins_6_bets(self):
        response = self.client.post(self.url, {"player_amount": 1})

        self.assertContains(response, "Player amount: 6")
