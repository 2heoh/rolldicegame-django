from django.apps import AppConfig


class RolldicegameConfig(AppConfig):
    name = 'rolldicegame'
